'''
Created on 17 apr. 2013

@author: puredevotion

@TODO: Json output
       multiprocessing: P1 ->readline from csvreader, check >1989
                        P2 ->get line from P1, sanitize
                        P3 ->process line, insert into ngramDict
                        if P3 done? write file.
'''
import csv
import sys
import json
import cProfile, pstats, io
from multiprocessing import Process, Queue, Manager
from multiprocessing.process import current_process
import os
from datetime import datetime

global term_list
global freq_file
global ngram_file
global output_file
global term_list

    #'''
    #Ngram opens a frequency list and checks if ngrams are part of that frequency list
    #'''

def init(rFreq_file="frequency_list_wf.csv", rNgram_file="aw-test", rOutput_file="aw-test.txt", gramNum=4):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global freq_file
    global ngram_file
    global output_file

    output_file = rOutput_file
    freq_file = rFreq_file
    ngram_file = rNgram_file

    if __name__ == "__main__":
        print("main started")
        info('main line')

        man = Manager()
        ngramDict = man.dict()
        #print(ngramDict)
        readline_queue = Queue(10000000)
        sanitize_queue = Queue(10000000)

        readlineProces = Process(target=readlines, args=(readline_queue,))
        sanitizeProces = Process(target=sanitize, args=(readline_queue, sanitize_queue,gramNum,))
        addNgramProces = Process(target=insert_ngram, args=(sanitize_queue,ngramDict,))
        sanitizeProces.start()
        readlineProces.start()
        addNgramProces.start()
        addNgramProces.join()
        #print(ngramDict)
        readlineProces.join()
        readline_queue.stop()
        sanitizeProces.join()
        sanitize_queue.stop()
        write_to_file(ngramDict)

    pass

def read_freq_terms(file):
    '''
    opens the frequency list and loads it in a set
    '''
    global freq_file
    print("reading the frequency list...")

    try:
        with open(file, 'r', encoding='utf8') as freq_file:
            csvreader = csv.reader(freq_file, delimiter=';', quoting=csv.QUOTE_NONE)

            term_list = set()
            for row in csvreader:
                word = row[0]
                term_list.add(word)
    except IOError:
        sys.exit("cannot open {0}".format(file))
    except csv.Error as e:
        sys.exit('line {}: {}'.format(reader.line_num, e))
    else:
        print("imported frequency list")
        print("number of terms: {0}".format(len(term_list)))

    return term_list

def readlines(readline_queue):
    global ngram_file
    try:
        with open(ngram_file, 'r', encoding='utf-8') as ngram_file:
            ngram_reader = open_ngram_file(ngram_file)

            for line in ngram_reader:
                if int(line[1]) > 1989:
                    #print(line)
                    readline_queue.put(line)
            print("All lines read!")
            readline_queue.put(None)
            return
    except IOError:
        sys.exit("cannot open {0}".format(ngram_file))
    except Exception as e:
        readline_queue.put("%s readline failed with: %s" % (current_process().name, e))

def open_ngram_file(file):
    '''
    opens a ngram file and returns the file handle
    '''
    print("opening ngram file...")

    try:
        csv.QUOTE_NONE
        dialect = csv.Sniffer().sniff(file.read(4096), delimiters='\t')
        file.seek(0)
        csvreader = csv.reader(file, dialect)
    except IOError:
        sys.exit("cannot open {0}".format(file))
    except csv.Error as e:
        sys.exit('line {}: {}'.format(csvreader.line_num, e))
    finally:
        return csvreader

def sanitize(readline_queue, sanitize_queue, ngramNum):
    term_list = read_freq_terms(freq_file)
    try:
        for line in iter(readline_queue.get, None):
            item = line[0].lower()
            #print(item)
            word_list = item.split()
            ngram = word_list[0].split("_")[0]
            if(ngram in term_list):
                frequency = line[2]
                for i in range(1, ngramNum+1):
                    word_list[i]=word_list[i].split("_")[0]

                if(word_list[1] in term_list) and (word_list[2] in term_list) and (word_list[3] in term_list) and (word_list[4] in term_list):
                    word_list.append(frequency)
                    print(word_list)
                    sanitize_queue.put(word_list)

        print('sanitized all lines...')
        sanitize_queue.put(None)
        return
    except Exception as e:
        sanitize_queue.put("%s sanitize failed with: %s" % (current_process().name, e))

def insert_ngram(sanitize_queue, ngramDict):
    ngramiDict = Vividict()
    ngramiDict.update(ngramDict)
    try:
        for word_list in iter(sanitize_queue.get, None):
            if ngramiDict[word_list[0]][word_list[1]][word_list[2]][word_list[3]][word_list[4]]:
                ngramiDict[word_list[0]][word_list[1]][word_list[2]][word_list[3]][word_list[4]]+=int(word_list[5])
            else:
                ngramiDict[word_list[0]][word_list[1]][word_list[2]][word_list[3]][word_list[4]]=int(word_list[5])
        print('Inserted all ngrams...')
        #print(ngramiDict)
        ngramDict.update(ngramiDict)
        #write_to_file(ngramiDict)
        return
    except KeyError as e:
        print("Key %s not found in %s" % (e, ngramiDict))
    except Exception as e:
        print("%s failed ngram with: %s" % (current_process().name, e))


def write_to_file(ngramDict):
    global output_file
    localNgram = {}
    localNgram.update(ngramDict)
    try:
        with open(output_file, 'w', encoding='utf8') as output_file:
            json.dump(localNgram, output_file, indent=4)
        print("Succesfully wrote all ngrams to output file!")
        print(datetime.now())
    except IOError as err:
        sys.exit("I/O error: {0}".format(err))

def info(title):
    '''
    Return info on the process this function is run at
    '''
    print(title)
    print('\tmodule name:', __name__)
    if hasattr(os, 'getppid'):  # only available on Unix
        print('\tparent process:', os.getppid())
    print('\tprocess id:', os.getpid())

def file_len(f):
    '''
    count the lines in the file, without opening the full thing
    '''
    i=0
    for i, l in enumerate(f):
        pass
    return i + 1

class Vividict(dict):
    """Implementation of perl's autovivification feature.
       borrowed from: http://stackoverflow.com/questions/635483/what-is-the-best-way-to-implement-nested-dictionaries-in-python/19829714#19829714
       and from: http://stackoverflow.com/questions/635483/what-is-the-best-way-to-implement-nested-dictionaries-in-python
    """
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


#pr = cProfile.Profile()
#pr.enable()
ngram = init()
#pr.disable()
#s = io.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats()
#print(s.getvalue())
