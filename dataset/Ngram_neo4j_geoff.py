'''
Created on 2 jan. 2013

@author: puredevotion
'''
import os
import sys
import json
import linecache
import traceback
from datetime import datetime
from pprint import pprint
import cProfile, pstats, io

n=0
geoff_list = []
output_file = ''

def read_dir(path='.'):
    print("reading directory listing in ", path)
    dir_list = []
    for fn in os.listdir(path):
        if os.path.isfile(fn) and fn.endswith('.json'):
            dir_list.append(fn)

    print("Found {0} files".format(len(dir_list)))
    for f in dir_list:
        f_ext = f.split(".")[0]
        init(rNgram_file=f, rOutput_file=f_ext+".geoff")


def init(rNgram_file="ag.json", rOutput_file="ag.geoff"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global output_file
    output_file = rOutput_file
    global geoff_list
    
    try:
        with open(rNgram_file, 'r', encoding='utf-8') as ngram_file:
            data = json.load(ngram_file)
            geoff_list = []
            print(datetime.now())
            print("reading " + rNgram_file)
            add_items(data)
            del data
            write_to_file()
            del geoff_list
            ngram_file.close()
    except IOError:
        sys.exit("cannot open {0}".format(rNgram_file))


def add_items(data):
    '''
    calculate the probability of al the ranks
    '''
    global n
    global geoff_list

    try:
        for ngram, one_grams in data.items():
            # ngram = aesthetic, 1gram = feeling
            if ngram == '_rank' or one_grams == '_rank' or ngram == '_prob' or one_grams == '_prob':
                continue

            n += 1;
            ngram_id = str(n)
            ngram_rank = int(one_grams['_rank'])
            # float precision of 7 can be low, but fits in a 32bit floating point
            # if prob(1) and prob(2) == equal, then fall back to rank
            ngram_prob = '%.7f' % float(one_grams['_prob'])
            geoff_list.append("("+ngram_id+" :0gram {\"word\": \""+ngram+"\", \"rank\": "+str(ngram_rank)+", \"prob\": "+ngram_prob+", \"gram\": \"0gram\", \"nid\": "+ngram_id+"})")

            for one_gram, two_grams in one_grams.items():
                # 1gram = feeling, 2gram = the
                if one_gram == '_rank' or one_gram == '_prob' or two_grams == '_prob' or two_grams == '_rank':
                    continue

                n += 1;
                one_gram_id = str(n)
                one_gram_rank = int(two_grams['_rank'])
                one_gram_prob = '%.7f' % float(two_grams['_prob'])
                geoff_list.append("("+one_gram_id+" :1gram {\"word\": \""+one_gram+"\", \"rank\": "+str(one_gram_rank)+", \"prob\": "+one_gram_prob+", \"gram\": \"1gram\", \"nid\": "+one_gram_id+"})")
                geoff_list.append("("+ngram_id+")-[:FOLLOWED_BY]->("+one_gram_id+")")

                for two_gram, three_grams in two_grams.items():
                    # 2gram = the, 3gram = feeling
                    if two_gram == '_rank' or two_gram == '_prob' or three_grams == '_prob' or three_grams == '_rank':
                        continue

                    n += 1;
                    two_gram_rank = int(three_grams['_rank'])
                    two_gram_prob = '%.7f' % float(three_grams['_prob']) 
                    two_gram_id = str(n)
                    geoff_list.append("("+two_gram_id+" :2gram {\"word\": \""+two_gram+"\", \"rank\": "+str(two_gram_rank)+", \"prob\": "+two_gram_prob+", \"gram\": \"2gram\", \"nid\": "+two_gram_id+"})")
                    geoff_list.append("("+one_gram_id+")-[:FOLLOWED_BY]->("+two_gram_id+")")

                    for three_gram, four_grams in three_grams.items():
                        # 3gram  = feeling, 4gram = of
                        if three_gram == '_rank' or three_gram == '_prob' or four_grams == '_prob' or four_grams == '_rank':
                            continue

                        n += 1;
                        three_gram_rank = int(four_grams['_rank'])
                        three_gram_prob = '%.7f' % float(four_grams['_prob'])
                        three_gram_id = str(n)
                        geoff_list.append("("+three_gram_id+" :3gram {\"word\": \""+ngram+"\", \"rank\": "+str(ngram_rank)+", \"prob\": "+ngram_prob+", \"gram\": \"3gram\", \"nid\": "+three_gram_id+"})")
                        geoff_list.append("("+two_gram_id+")-[:FOLLOWED_BY]->("+three_gram_id+")")

                        for four_gram, values in four_grams.items():
                            # 4gram = of, values = 34
                            if four_gram == '_rank' or four_gram == '_prob':
                                continue

                            n += 1;
                            four_gram_id = str(n)
                            four_gram_rank = int(values['_rank'])
                            four_gram_prob = '%.7f' % float(values['_prob'])
                            geoff_list.append("("+four_gram_id+" :4gram {\"word\": \""+four_gram+"\", \"rank\": "+str(four_gram_rank)+", \"prob\": "+four_gram_prob+", \"gram\": \"4gram\", \"nid\": "+four_gram_id+"})")
                            geoff_list.append("("+three_gram_id+")-[:FOLLOWED_BY]->("+four_gram_id+")")
    
        print("All nodes converted to GEOFF.")
        del data

    except IndexError:
        PrintException()
    except AttributeError:
        PrintException()
    except EOFError:
        PrintException()
    except TypeError:
        PrintException()
    except:
        PrintException()


def write_to_file():
    global output_file
    global geoff_list
    try:
        with open(output_file, 'w', encoding='utf8') as output:
            for line in geoff_list:
                output.write("%s\n" % line)
        print("Succesfully wrote all geoff lines to {0}!".format(str(output_file)))
    except IOError as err:
        sys.exit("I/O error: {0}".format(err))

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj,tb)))
    print("*** extract_tb:")
    print(repr(traceback.extract_tb(tb)))
    print("*** format_tb:")
    print(repr(traceback.format_tb(tb)))
    print("*************************************")
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))


#init(rNgram_file="dataset_id.json", rOutput_file="dataset_id.geoff")
read_dir()
#filelist = ["ro","ru","no"]
# for i in filelist:
