'''
Created on 17 apr. 2013

@author: puredevotion
'''
import csv
import re
import sys
import redis
from redis_collections import Dict

class Ngram(object):
    '''
    Ngram opens a frequency list and checks if ngrams are part of that frequency list
    '''

    def __init__(self, rFreq_file="frequency_list_wf.csv", rNgram_file="googlebooks-eng-all-2gram-20120701-am.txt", rOutput_file="googlebooks-eng-all-2gram-am.txt", gramNum=1):
        '''
        Constructor, creates new file, creates freq_list, opens a Redis connection et al.
        '''
        global str = str
        global unicode = str
        global bytes = bytes
        global basestring = (str,bytes)

        self.r = redis.StrictRedis("localhost")
        
        self.output_file = rOutput_file

        self.term_list = self.read_freq_terms(rFreq_file)

        try:
            with open(rNgram_file, 'r', encoding='utf-8') as ngram_file:
                self.ngram_reader = self.open_ngram_file(ngram_file)
                self.check_ngram_in_freq_list(gramNum)
        except IOError:
            sys.exit("cannot open {0}".format(rNgram_file))

        pass

    def file_len(self, f):
        '''
        count the lines in the file, without opening the full thing
        only useful for multi-threading
        '''
        i=0
        for i, l in enumerate(f):
            pass
        return i + 1

    def read_freq_terms(self, file):
        '''
        opens the frequency list and loads it in a set
        '''
        print("reading the frequency list...")

        try:
            with open(file, 'r', encoding='utf8') as freq_file:
                csvreader = csv.reader(freq_file, delimiter=';', quoting=csv.QUOTE_NONE)

                self.term_list = set()
                for row in csvreader:
                    word = row[0].lower()
                    self.term_list.add(word)
        except IOError:
            sys.exit("cannot open {0}".format(file))
        except csv.Error as e:
            sys.exit('line {}: {}'.format(reader.line_num, e))
        else:
            print("imported frequency list")
            print("number of terms: {0}".format(len(self.term_list)))

        return self.term_list

    def open_ngram_file(self, file):
        '''
        opens a ngram file, counts the amount of lines and returns the handle
        '''
        print("opening ngram file...")

        try:
            num_lines = self.file_len(file)

            dialect = csv.Sniffer().sniff(file.read(1024))
            file.seek(0)
            csvreader = csv.reader(file, dialect)
        except IOError:
            sys.exit("cannot open {0}".format(file))
        except csv.Error as e:
            sys.exit('line {}: {}'.format(csvreader.line_num, e))
        finally:
            if num_lines < 1:
                print("less than 1 line!")
            else:
                print("number of lines: {0}".format(num_lines))

            return csvreader

    def open_output_file(self, file):
        print("opening output file...")

        try:
            output_file = open(file, encoding='utf-8', mode='a+')
            #with open('output', encoding='utf-8', mode='a+') as f:
                #num_lines = self.file_len(f)
                #print("File length: {0}".format(num_lines))
            #    self.output = f
        except IOError as err:
            sys.exit('cannot create new file -I/O error: {0}'.format(err))
        else:
            if self.file_len(output_file) < 2:
                print("file opened!")

            return output_file

    def check_ngram_in_freq_list(self, ngramNum):
        '''
        check if ngram is in frequency list.
        readline and writes corresponding ngrams to new ngram file

        we strip all quotes, capitals, tags and others in cleanup.
        we split each line on the column (google seems to be using spaces and tabs)
        '''
        print("checking ngram in freq list...")

        cleanup = re.compile(r"(_[A-Za-z\_\-]+)|(\")")
        column_splitter = re.compile(r"\s")

        print("gogogo ")
        self.ngramDict = Dict(redis=r)
        for line in self.ngram_reader:
            item = line[0].encode('utf-8', errors='ignore')
            item = item.decode('ascii', errors='ignore')
            item = item.lower().strip()
            item = cleanup.sub("", item)

            word_list = column_splitter.split(item)
            for word in word_list:
                word = word.lower().strip()
                word = cleanup.sub("", word)

            if int(line[1]) > 1989:
                if(word_list[0] in self.term_list):
                    ngram = word_list[0]
                    frequency = line[2]
                    if ngramNum > 0:
                        temp = []
                        for i in range(1, ngramNum+1):
                            if(i<len(word_list)):
                                if(word_list[i] in self.term_list):
                                    temp.append(word_list[i])
                        if temp:
                            ngramFolowers = ' '.join(temp)
                            key = ngram+" "+ngramFolowers
                            if self.ngramDict.__contains__(key):
                                freq = self.ngramDict.get(key)
                                freq = int(freq)+int(frequency)
                                self.ngramDict.update({key: freq})
                            else:
                                self.ngramDict.update({key: frequency})

        self.write_to_file()                        
                            
    def write_to_file(self):
        try:
            with open(self.output_file, 'a+', encoding='utf8') as output_file:
                for k, v in self.ngramDict.items():
                    split = k.split(" ")
                    ngram = split[0]
                    ngramFolowers = split[1]
                    frequency = v
                    output_file.write("\""+ngram+"\", \""+ngramFolowers+"\", \""+str(frequency)+"\"\n")
                print("write actie success!")
        except IOError as err:
            sys.exit("I/O error: {0}".format(err))
