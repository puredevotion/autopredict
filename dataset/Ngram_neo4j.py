'''
Created on 2 jan. 2013

@author: puredevotion
'''
#import collections
import os
import sys
import json
import linecache
import traceback
from datetime import datetime
from py2neo import neo4j, node, rel
#from pprint import pprint
#import cProfile, pstats, io

graph_db = neo4j.GraphDatabaseService("http://127.0.0.1:7474/db/data/")
n=0

def read_dir(path='.'):
    print("reading directory listing in ", path)
    dir_list = []
    for fn in os.listdir(path):
        if os.path.isfile(fn) and fn.endswith('.json'):
            dir_list.append(fn)

    print("Found {0} files".format(len(dir_list)))
    for f in dir_list:
        init(rNgram_file=f)


def init(rNgram_file="ag.txt"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    graph_db.delete_index(neo4j.Node, "temp")
    try:
        with open(rNgram_file, 'r', encoding='utf-8') as ngram_file:
            data = json.load(ngram_file)
            print(datetime.now())
            print("reading " + rNgram_file)
            add_items(data)
            ngram_file.close()
            del data
    except IOError:
        sys.exit("cannot open {0}".format(rNgram_file))


def add_items(data):
    '''
    calculate the probability of al the ranks
    '''
    global batch
    global session
    global n

    # create indices for faster search
    two_gram_index = graph_db.get_or_create_index(neo4j.Node, "temp")

    try:
        for ngram, one_grams in data.items():
            # ngram = aesthetic, 1gram = feeling
            if ngram == '_rank' or one_grams == '_rank' or ngram == '_prob' or one_grams == '_prob':
                continue

            n += 1;
            ngram_rank = int(one_grams['_rank'])
            # float precision of 7 can be low, but fits in a 32bit floating point
            # if prob(1) and prob(2) == equal, then fall back to rank
            ngram_prob = '%.7f' % float(one_grams['_prob'])
            ngram_node, = graph_db.create({"word": ngram, "rank": ngram_rank, "prob": ngram_prob, "gram": '0gram', "nid": n})
            ngram_node.add_labels("0gram")

            for one_gram, two_grams in one_grams.items():
                # 1gram = feeling, 2gram = the
                if one_gram == '_rank' or one_gram == '_prob' or two_grams == '_prob' or two_grams == '_rank':
                    continue

                n += 1;
                one_gram_rank = int(two_grams['_rank'])
                one_gram_prob = '%.7f' % float(two_grams['_prob'])
                one_gram_node, = graph_db.create(node({"word": one_gram, "rank": str(one_gram_rank), "prob": one_gram_prob, "gram": "1gram", "nid": n}))
                one_gram_node.add_labels("1gram")
                graph_db.create(rel((ngram_node, "FOLLOWED_BY", one_gram_node)))

                for two_gram, three_grams in two_grams.items():
                    # 2gram = the, 3gram = feeling
                    if two_gram == '_rank' or two_gram == '_prob' or three_grams == '_prob' or three_grams == '_rank':
                        continue

                    n += 1;
                    # We batch all 2-4grams in one request for speed.
                    # let's create a batch instance
                    batch = neo4j.WriteBatch(graph_db)

                    # Add 2gram node to batch, including labels and index it
                    two_gram_rank = int(three_grams['_rank'])
                    two_gram_prob = '%.7f' % float(three_grams['_prob']) 
                    two_gram_node = batch.create(node({"word": two_gram, "rank": two_gram_rank, "prob": two_gram_prob, "gram": '2gram', "nid": n}))
                    two_gram_id = n
                    batch.add_labels(two_gram_node, "2gram")
                    batch.add_indexed_node(two_gram_index, "id", n, two_gram_node)

                    for three_gram, four_grams in three_grams.items():
                        # 3gram  = feeling, 4gram = of
                        if three_gram == '_rank' or three_gram == '_prob' or four_grams == '_prob' or four_grams == '_rank':
                            continue

                        n += 1;
                        three_gram_rank = int(four_grams['_rank'])
                        three_gram_prob = '%.7f' % float(four_grams['_prob'])
                        three_gram_node = batch.create(node({"word": ngram, "rank": str(ngram_rank), "prob": str(ngram_prob), "gram": "3gram", "nid": n}))
                        batch.add_labels(three_gram_node, "3gram")
                        batch.create(rel((two_gram_node, "FOLLOWED_BY", three_gram_node)))

                        for four_gram, values in four_grams.items():
                            # 4gram = of, values = 34
                            if four_gram == '_rank' or four_gram == '_prob':
                                continue

                            n += 1;
                            four_gram_rank = int(values['_rank'])
                            four_gram_prob = '%.7f' % float(values['_prob'])
                            four_gram_node = batch.create(node({"word": four_gram, 
                                                          "rank": four_gram_rank, "prob": four_gram_prob, "gram": '4gram', "nid": n}))
                            batch.add_labels(four_gram_node, "4gram")
                            batch.create(rel((three_gram_node, "FOLLOWED_BY", four_gram_node)))
                    
                    # two_gram for loop
                    batch.run()
                    two_gram_node_ref = graph_db.get_indexed_node("temp", "id", two_gram_id)            
                    graph_db.create(rel((one_gram_node, "FOLLOWED_BY", two_gram_node_ref)))

    except IndexError:
        PrintException()
    except cypher.TransactionError as e:
        print("--------------------------------------------------------------------------------------------")
        PrintException()
    except AttributeError:
        PrintException()

    except EOFError:
        PrintException()

    except TypeError:
        PrintException()
    except:
        PrintException()


    print("All nodes imported.")
    del data


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj,tb)))
    print("*** extract_tb:")
    print(repr(traceback.extract_tb(tb)))
    print("*** format_tb:")
    print(repr(traceback.format_tb(tb)))
    print("*************************************")
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))

#pr = cProfile.Profile()
#pr.enable()
#init(rNgram_file="dataset_id.json")
#pr.disable()
#s = io.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats()
#print(s.getvalue())

graph_db.clear()
read_dir()
#filelist = ["ro","ru","no"]
# for i in filelist:
