'''
Created on 17 apr. 2013

@author: puredevotion

@TODO: Json output
       multiprocessing: P1 ->readline from csvreader, check >1989
                        P2 ->get line from P1, sanitize
                        P3 ->process line, insert into ngramDict
                        if P3 done? write file.
'''
import csv
import sys
import json
import cProfile, pstats, io
from datetime import datetime

class Ngram(object):
    '''
    Ngram opens a frequency list and checks if ngrams are part of that frequency list
    '''

    def __init__(self, rFreq_file="frequency_list_wf.csv", rNgram_file="aw-test", rOutput_file="aw-test.txt", gramNum=4):
        '''
        Constructor, creates new file, creates freq_list et al.
        '''
        self.output_file = rOutput_file
        self.term_list = self.read_freq_terms(rFreq_file)

        try:
            with open(rNgram_file, 'r', encoding='utf-8') as ngram_file:
                self.ngram_reader = self.open_ngram_file(ngram_file)
                self.check_ngram_in_freq_list(gramNum)
        except IOError:
            sys.exit("cannot open {0}".format(rNgram_file))

        pass

    def file_len(self, f):
        '''
        count the lines in the file, without opening the full thing
        could be used for multithreading/multiprocessing
        '''
        i=0
        for i, l in enumerate(f):
            pass
        return i + 1

    def read_freq_terms(self, file):
        '''
        opens the frequency list and loads it in a set
        '''
        print("reading the frequency list...")

        try:
            with open(file, 'r', encoding='utf8') as freq_file:
                csvreader = csv.reader(freq_file, delimiter=';', quoting=csv.QUOTE_NONE)

                self.term_list = set()
                for row in csvreader:
                    word = row[0]
                    self.term_list.add(word)
        except IOError:
            sys.exit("cannot open {0}".format(file))
        except csv.Error as e:
            sys.exit('line {}: {}'.format(reader.line_num, e))
        else:
            print("imported frequency list")
            print("number of terms: {0}".format(len(self.term_list)))

        return self.term_list

    def open_ngram_file(self, file):
        '''
        opens a ngram file, counts the amount of lines and returns the handle
        '''
        print("opening ngram file...")

        try:
            csv.QUOTE_NONE
            dialect = csv.Sniffer().sniff(file.read(4096), delimiters='\t')
            file.seek(0)
            csvreader = csv.reader(file, dialect)
        except IOError:
            sys.exit("cannot open {0}".format(file))
        except csv.Error as e:
            sys.exit('line {}: {}'.format(csvreader.line_num, e))
        finally:
            return csvreader

    def check_ngram_in_freq_list(self, ngramNum):
        '''
        check if ngram is in frequency list.
        readline and writes corresponding ngrams to new ngram file

        we strip all quotes, capitals, tags and others in cleanup.
        we split each line on the column (google seems to be using spaces and tabs)
        '''
        print("processing ngrams...")

        self.ngramDict = Vividict()
        for line in self.ngram_reader:
            if int(line[1]) > 1989:
                #item = line[0].encode('utf-8', errors='ignore')
                #item = item.decode('ascii', errors='ignore')
                item = line[0].lower()#.strip()

                word_list = item.split()
                ngram = word_list[0].split("_")[0]
                if(ngram in self.term_list):
                    frequency = line[2]
                    for i in range(1, ngramNum+1):
                        word_list[i]=word_list[i].split("_")[0]

                    if(word_list[1] in self.term_list) and (word_list[2] in self.term_list) and (word_list[3] in self.term_list) and (word_list[4] in self.term_list):
                        if self.ngramDict[ngram][word_list[1]][word_list[2]][word_list[3]][word_list[4]]:
                            self.ngramDict[ngram][word_list[1]][word_list[2]][word_list[3]][word_list[4]]+=int(frequency)
                        else:
                            self.ngramDict[ngram][word_list[1]][word_list[2]][word_list[3]][word_list[4]]=int(frequency)
        self.write_to_file()

    def write_to_file(self):
        try:
            with open(self.output_file, 'w', encoding='utf8') as output_file:
                json.dump(self.ngramDict, output_file, indent=4)
            print("Succesfully wrote all ngrams to output file!")
            print(datetime.now())
        except IOError as err:
            sys.exit("I/O error: {0}".format(err))

class Vividict(dict):
    """Implementation of perl's autovivification feature.
       borrowed from: http://stackoverflow.com/questions/635483/what-is-the-best-way-to-implement-nested-dictionaries-in-python/19829714#19829714
       and from: http://stackoverflow.com/questions/635483/what-is-the-best-way-to-implement-nested-dictionaries-in-python
    """
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


pr = cProfile.Profile()
pr.enable()
ngram = Ngram()
pr.disable()
s = io.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print(s.getvalue())
