'''
Created on 16 dec. 2013

@author: puredevotion
'''

import sys
import json
import linecache
import traceback
from datetime import datetime
from pprint import pprint

data_list = []
n = 0


def init(data_file="data_13-7.json"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global data_list
    global n

    try:
        with open(data_file, 'r') as fh:
            data = json.load(fh)
            print("reading " + data_file + " @ ", datetime.now())
            data_list.append(
                "\"id\",\"email\",\"month\",\"year\",\"english\",\"gender\",\"handedness\",\"keyboard\",\"typeskill\",\"ui\",\"words\",\"control_duration\",\"treatment_duration\",\"s2_duration\",\"s2_start\",\"s2_end\",\"s4_duration\",\"s4_start\",\"s4_end\",\"improvement\",\"general\",\"accuracy\",\"speed\",\"comments\"")
            for root in data:
#                pprint(root)
#                input()
                month = year = english = typeskill = accuracy = general = speed = improvement = ui = words = 0
                s2_end = s2_start = s2_duration = s4_end = s4_start = s4_duration = control_duration = treatment_duration = 0.0
                keyboard = gender = handedness = comments = email = ""

                print("-------------" + str(n) + "--------------------")
                for k, v in root.items():
                    if k == 'info':
                        email = v['email']
                        gender = v['gender']
                        handedness = v['handedness']
                        keyboard = v['keyboard']
                        typeskill = v['typeskill']
                        english = v['englishskill']
                        month = v['birthdate'].split("-")[0]
                        year = v['birthdate'].split("-")[1]
                    elif k == 'step2':
#                        pprint(k)
#                        pprint(v)
                        s2_end = v['stop']
                        s2_start = v['start']
                        s2_duration = v['duration']
                        if 'text' in v:
                            start = v['text'][0]['timestamp']
                            stop = v['text'][-1]['timestamp']
                            control_duration = float(
                                stop) - float(start)
                    elif k == 'step4':
#                        pprint(k)
#                        pprint(v)
                        if n == 28:
                            pprint(v)
                        s4_end = v['stop']
                        s4_start = v['start']
                        s4_duration = v['duration']
                        if 'text' in v:
                            start2 = v['text'][0]['timestamp']
                            stop2 = v['text'][-1]['timestamp']
                            treatment_duration = float(
                                stop2) - float(start2)
                    elif k == 'experience':
#                        pprint(k)
#                        pprint(v)
                        if isinstance(v, dict):
                            pprint(v)
                            if 'accuracy' in v:
                                accuracy = v['accuracy']
                            if 'general' in v:
                                general = v['general']
                            if 'improvement' in v:
                                improvement = v['improvement']
                            if 'speed' in v:
                                speed = v['speed']
                            if 'comments' in v:
                                comments = v['comments']
                    elif k == 'config':
#                        pprint(k)
#                        pprint(v)
                        ui = v['UIcase']
                        words = v['predictionDepth']

                data_list.append("\"" + str(n) + "\",\"" + email + "\",\"" + str(month) + "\",\"" + str(year) + "\",\"" + str(english) + "\",\"" + str(gender) + "\",\"" + handedness + "\",\"" + keyboard + "\",\"" + str(typeskill) + "\",\"" + str(ui) + "\",\"" + str(words) + "\",\"" + str(control_duration) + "\",\"" + str(
                    treatment_duration) + "\",\"" + str(s2_duration) + "\",\"" + str(s2_start) + "\",\"" + str(s2_end) + "\",\"" + str(s4_duration) + "\",\"" + str(s4_start) + "\",\"" + str(s4_end) + "\",\"" + str(improvement) + "\",\"" + str(general) + "\",\"" + str(accuracy) + "\",\"" + str(speed) + "\",\"" + comments + "\"")
#                input()
                n += 1
            del data
            writewrite()
    except:
        PrintException()
    fh.close()


def writewrite():
    global data_list
    try:
        with open("data_time_results.csv", 'w', encoding='utf8') as output:
            for line in data_list:
                output.write("%s\n" % line)
        print("Succesfully wrote all nodes to {0}!".format(
            str("data_time_results.csv")))
        print(datetime.now())

    except IOError as err:
        sys.exit("I/O error: {0}".format(err))


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj, tb)))
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))


# read_dir()
init("data_13-7.json")
# write_to_file()
