from multiprocessing import Process, Queue, Array, Manager
from multiprocessing.process import current_process
import os

# get worker1 to proces numbers and put in queue1 (line reading)
# get worker2 to get number from queue1 and put numbers in queue2 (sanitization)
# get worker3 to get numbers from queue2 and add to dict.
# worker3 done? Join() and print dict

def info(title):
    print(title)
    print('module name:', __name__)
    if hasattr(os, 'getppid'):  # only available on Unix
        print('parent process:', os.getppid())
    print('process id:', os.getpid())

def readlines(queue1):
    try:
        for n in range(1,100):
            print("readlines "+str(n))
            queue1.put(n)

        print("readlines is done")
        queue1.put(None)

    except Exception as e:
         queue1.put("%s failed with: %s" % (current_process().name, e.message))

def sanitize(queue1, queue2):
    try:
        for number in iter(queue1.get, None):
            print("sanitize "+str(number))
            queue2.put(number)
        print('sanitize is done')
        queue2.put(None)
    except Exception as e:
        queue2.put("%s failed with: %s" % (current_process().name, e.message))

def processor(queue2, finalList):
    try:
        for number in iter(queue2.get, None):
            print("proc "+str(number))
            finalList.append(number)
        print('processor is done')
        NOTDONE = False;

        return
    except Exception as e:
        print("%s failed with: %s" % (current_process().name, e.message))

if __name__ == "__main__":

    print("main started")
    info('main line')
    man = Manager()
    finalList = man.list()

    queue1 = Queue()
    queue2 = Queue()

    p1 = Process(target=readlines, args=(queue1,))
    p2 = Process(target=sanitize, args=(queue1, queue2,))
    p3 = Process(target=processor, args=(queue2, finalList,))
    p1.start()
    p2.start()
    p3.start()
    print("voor")
    p3.join()
    print("na")
    print(finalList)
