'''
Created on 16 dec. 2013

@author: puredevotion
'''
import os
import sys
import json
import linecache
import traceback
from datetime import datetime
from pprint import pprint
from couchbase import Couchbase

rankings = {}
files = {}
som = 0


def read_dir(path='.'):
    global files

    print("reading directory listing in ", path)
    dir_list = []
    for fn in os.listdir(path):
        if os.path.isfile(fn) and fn.endswith('.json'):
            dir_list.append(fn)

    print("Found {0} files".format(len(dir_list)))
    for f in dir_list:
        init(f)
    print(str(len(rankings))+" total sum: "+str(som))
    write_to_file()
    del rankings
    del files
    del som


def init(rank_file="ag.txt"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global files

    cb = Couchbase.connect(bucket='default',  host='127.0.0.1')

    try:
        with open(rank_file, 'r') as fh:
            data = json.load(fh)
            print("reading " + rank_file + "  @  ", datetime.now())
            for kr, root in data.items():
                cb.set(kr, root)
            del data

    except:
        PrintException()
    fh.close()

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj, tb)))
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))


#read_dir()
init("dataset_aa.json")
#write_to_file()
