'''
Created on 17 apr. 2013

@author: puredevotion
'''
import collections
import os
import sys
import json
import linecache
import traceback
from datetime import datetime

global output_file


def read_dir(path='.'):
    print("reading directory listing in ", path)
    dir_list = []
    for fn in os.listdir(path):
        if os.path.isfile(fn) and fn.endswith('.txt'):
            dir_list.append(fn)

    print("Found {0} files".format(len(dir_list)))
    for f in dir_list:
        f_ext = f.split(".")[0]
        init(rNgram_file=f, rOutput_file=f_ext+".json")


def init(rNgram_file="ag.txt", rOutput_file="ag.json"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global output_file
    output_file = rOutput_file

    try:
        with open(rNgram_file, 'r', encoding='utf-8') as ngram_file:
            data = json.load(ngram_file)
            print(datetime.now())
            print("reading " + rNgram_file)
            data = rank_items(data)
            data = probability_items(data)
            write_to_file(data)
            ngram_file.close()
            del data
    except IOError:
        sys.exit("cannot open {0}".format(rNgram_file))


def rank_items(data):
    '''
    Rank all ngrams found in a file
    Each file has a tree (in JSON) with 0-4grams.
    These are all ranked, and then a JSON file is Created
    Ranking is counting the occurences of an underlying group of ngrams
    '''

    ngram_rank = one_gram_rank = two_gram_rank = three_gram_rank = 0
    try:
        for ngram, one_grams in data.items():
            ngram_rank = 0
            # ngram = aesthetic, 1gram = feeling
            for one_gram, two_grams in one_grams.items():
                one_gram_rank = 0
                # 1gram = feeling, 2gram = the
                for two_gram, three_grams in two_grams.items():
                    # 2gram = the, 3gram = feeling
                    two_gram_rank = 0
                    for three_gram, four_grams in three_grams.items():
                        # 3gram  = feeling, 4gram = of
                        three_gram_rank = 0
                        if isinstance(four_grams, collections.Mapping):
                            for four_gram, values in four_grams.items():
                                # 4gram = of, values = 34
                                #print("four_gram "+four_gram+": ", values)
                                three_gram_rank += values
                                #input()
                        else:
                            print("----------------GEEN DICT----------------")
                        #print("3gram "+three_gram+": ", three_gram_rank)
                        four_grams['_rank'] = int(three_gram_rank)
                        two_gram_rank += three_gram_rank
                    #print("2gram "+two_gram+" : ", two_gram_rank)
                    three_grams['_rank'] = int(two_gram_rank)
                    one_gram_rank += two_gram_rank
                #print("1gram "+one_gram+" : ", one_gram_rank)
                two_grams['_rank'] = int(one_gram_rank)
                ngram_rank += one_gram_rank
            #print("ngram : ", ngram_rank)
            one_grams['_rank'] = int(ngram_rank)

    except IndexError as e:
        print("index bestaat niet: ", e)
        pass
    except AttributeError as e:
        print("attr error: ", e)
        pass
    except EOFError as e:
        print("eof: ", e)
        pass
    except:
        print("Unexpected error:", sys.exc_info()[0])
        pass

    #pprint(data)
    return data


def probability_items(data):
    '''
    calculate the probability of al the ranks
    '''

    try:
        for ngram, one_grams in data.items():
            # ngram = aesthetic, 1gram = feeling
            ngram_rank = int(one_grams['_rank'])
            if ngram == '_rank' or one_grams == '_rank':
                continue

            for one_gram, two_grams in one_grams.items():
                # 1gram = feeling, 2gram = the
                if one_gram == '_rank' or two_grams == '_rank':
                    continue
                one_gram_rank = str(two_grams['_rank'])

                for two_gram, three_grams in two_grams.items():
                    # 2gram = the, 3gram = feeling
                    if two_gram == '_rank' or three_grams == '_rank':
                        continue
                    two_gram_rank = str(three_grams['_rank'])

                    for three_gram, four_grams in three_grams.items():
                        # 3gram  = feeling, 4gram = of
                        if three_gram == '_rank' or four_grams == '_rank':
                            continue
                        three_gram_rank = str(four_grams['_rank'])

                        for four_gram, values in four_grams.items():
                            # 4gram = of, values = 34
                            if four_gram == '_rank' or values == '_rank':
                                continue

                            four_gram_prob = int(values) / int(three_gram_rank)
                            four_grams[four_gram] = {'_rank': values, '_prob': four_gram_prob}
                        three_gram_prob = int(three_gram_rank) / int(two_gram_rank)
                        four_grams['_prob'] = three_gram_prob
                    two_gram_prob = int(two_gram_rank) / int(one_gram_rank)
                    three_grams['_prob'] = two_gram_prob
                one_gram_prob = int(one_gram_rank) / int(ngram_rank)
                two_grams['_prob'] = one_gram_prob
            ngram_prob = int(ngram_rank) / int(ngram_rank)
            one_grams['_prob'] = ngram_prob

        #pprint(data)

    except IndexError:
        PrintException()
        pass
    except AttributeError:
        PrintException()
        pass
    except EOFError:
        PrintException()
        pass
    except TypeError:
        PrintException()
        pass
    except:
        PrintException()
        pass

    return data


def write_to_file(data):
    global output_file
    try:
        with open(output_file, 'w', encoding='utf8') as output:
            json.dump(data, output, indent=1)
        print("Succesfully wrote all ranks and probabilities to {0}!".format(str(output_file)))
    except IOError as err:
        sys.exit("I/O error: {0}".format(err))


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj,
                                          tb)))
    print("*** extract_tb:")
    print(repr(traceback.extract_tb(tb)))
    print("*** format_tb:")
    print(repr(traceback.format_tb(tb)))
    print("*** tb_lineno:", tb.tb_lineno)
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))


read_dir()
#filelist = ["ro","ru","no"]
# for i in filelist:
#init(rNgram_file="ae.json", rOutput_file="json.json")
