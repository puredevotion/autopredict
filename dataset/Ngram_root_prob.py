'''
Created on 16 dec. 2013

@author: puredevotion
'''
import os
import sys
import json
import linecache
import traceback
from datetime import datetime
from pprint import pprint

rankings = {}
files = {}
som = 0


def read_dir(path='.'):
    global rankings
    global files
    global som

    print("reading directory listing in ", path)
    dir_list = []
    for fn in os.listdir(path):
        if os.path.isfile(fn) and fn.endswith('.json'):
            dir_list.append(fn)

    print("Found {0} files".format(len(dir_list)))
    for f in dir_list:
        init(f)
    print(str(len(rankings))+" total sum: "+str(som))
    write_to_file()
    del rankings
    del files
    del som


def init(rank_file="ag.txt"):
    '''
    Constructor, creates new file, creates freq_list et al.
    '''
    global rankings
    global files
    global som

    try:
        with open(rank_file, 'r') as fh:
            data = json.load(fh)
            print("reading " + rank_file + " @ ", datetime.now())
            for kr, root in data.items():
                for k, v in root.items():
                    if k == '_rank':
                        print("---K---")
                        pprint(kr+": "+k)
                        print("---v---")
                        pprint(v)
                        print("---root---")
                        pprint(root)
                        input()
                        if kr in rankings:
                            print("**ERROR: "+kr+" found in dict!**")
                            input()
                        rankings[kr] = v
                        if rank_file not in files:
                            files[rank_file] = {}
                        files[rank_file][kr] = v
                        som += v
            del data

    except:
        PrintException()
    fh.close()


def write_to_file():
    global rankings
    global files
    global som

    print("starting to re-write all files")
    for rank_file in files:
        output_file = rank_file
        print(rank_file)
        try:
            with open(output_file, 'r', encoding='utf8') as output_file:
                data = json.load(output_file)
                for element in files[rank_file]:
                    rank = rankings[element]
                    prob = int(rank) / int(som)
                    data[element]['_prob'] = prob

            with open("dataset_"+rank_file, 'w', encoding='utf8') as output_file:
                json.dump(data, output_file, indent=1)
                del data

        except IOError as err:
            sys.exit("I/O error: {0}".format(err))

        print("Succesfully wrote all ngrams to output file!")
        print(datetime.now())


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print(repr(traceback.format_exception(exc_type, exc_obj, tb)))
    print('Exception on line {}: {}: {}'.format(lineno, line.strip(), exc_obj))


#read_dir()
init("ad.json")
#write_to_file()
