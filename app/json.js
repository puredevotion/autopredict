[{
    "id": "mysql",
    "value": "MySQL",
    "label": "MySQL",
    "term": "mysql"
}, {
    "id": "mysqli",
    "value": "MySQLi",
    "label": "MySQLi",
    "term": "mysqli"
}, {
    "id": "mariadb",
    "value": "MariaDB",
    "label": "MariaDB",
    "term": "mariadb"
}, {
    "id": "java",
    "value": "Java",
    "label": "java",
    "term": "java"
}, {
    "id": "javascript",
    "value": "javascript",
    "label": "javascript",
    "term": "javascript"
}]
