/*jshint -W110 */

function stopWatch() {
    /**
     * StopWatch is a stopwatch
     * checks for window.performance, fallback to Date()
     * includes prototype functions for pause and displaying time.
     */
    this.startTime = 0;
    this.stopTime = 0;
    this.elapsedTime = 0;
    this.running = false;

    if (window.performance.now) {
        this.performance = "enabled";
    } else {
        if (window.performance.webkitNow) {
            this.performance = "webkit";
        } else {
            this.performance = "disabled";
        }
    }

    this.timings = [];
}

/**
 * currenTime gets the currentTime
 * it inherits from StopWatch
 * @return {float}   time from window.performance (prefered) or date.getTime()
 */
stopWatch.prototype.currentTime = function() {
    if(this.performance === "enabled") {
        return window.performance.now();
    }
    if(this.performance === "disabled") {
        return new Date().getTime();
    }
    if(this.performance === "webkit") {
        return window.performance.webkitNow();
    }
};
stopWatch.prototype.start = function() {
    var currentTime = this.currentTime();
    if (!this.startTime) {
        this.startTime = currentTime;
    }
    this.timings.push({
        start: currentTime
    });
    this.running = true;
};
stopWatch.prototype.stop = function() {
    this.stopTime = this.currentTime();
    var lastTiming = this.timings.length - 1;
    this.timings[lastTiming].stop = this.stopTime;
    this.elapsedTime += this.timings[lastTiming].stop - this.timings[lastTiming].start;
    this.running = false;
};
stopWatch.prototype.getElapsedMilliseconds = function() {
    return this.elapsedTime;
};
stopWatch.prototype.getElapsedSeconds = function() {
    return this.getElapsedMilliseconds() / 1000;
};
stopWatch.prototype.printElapsed = function(name) {
    var currentName = name || 'Elapsed:';

    // console.log(currentName, '[' + this.getElapsedMilliseconds() + 'ms]', '[' + this.getElapsedSeconds() + 's]');
};

/**
 * isNumeric checks of input is numeric only
 * @param  {int}  n the input to-be checked
 * @return {Boolean}
 */

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
 * isString checks of input is a string
 * @param  {string}  value the input to-be checked
 * @return {Boolean}
 */

function isString(value) {
    stringClass = '[object String]';
    return typeof value === 'string' || toString.call(value) === stringClass;
}

/**
 * isUndefined checks if a value is undefined
 * @param  {object}  value to be checked
 * @return {Boolean}
 */

function isUndefined(value) {
    return typeof value === 'undefined';
}

/**
 * fillForm fills the form at step1.
 * It uses the filled fields from localStorage
 *
 * @return {none}
 */

function fillForm() {
    var formData = $.jStorage.get('user');
    formData.month = formData.birthdate.split("-")[0];
    formData.year = formData.birthdate.split("-")[1];

    $('input:radio[id=handedness][value=' + formData.handedness + ']').prop('checked', true);
    $('input:radio[id=keyboard][value=' + formData.keyboard + ']').prop('checked', true);
    $('input:radio[id=typeskill][value=' + formData.typeskill + ']').prop('checked', true);
    $('input:radio[id=englishskill][value=' + formData.englishskill + ']').prop('checked', true);
    $('input:radio[id=gender][value=' + formData.gender + ']').prop('checked', true);
    $('option[value=' + formData.month + ']').prop('selected', true);
    $('input#jaar').val(formData.year);
    $('input#email').val(formData.email);
}

/**
 * getKey converts the key code to the representation of that code
 * special cases for modifiers, which get added to the representation.
 * @param  {int} key      the keycode
 * @param  {boolean} shiftKey if the shift-key was pressed
 * @param  {boolean} altKey   if the alt-key was pressed
 * @param  {boolean} ctrlKey  if the ctrl-key was pressed
 * @return {string}          the complete representation of the key incl. modifiers.
 */

function getKey(e, timestamp) {
    var key = e.which,
        shiftKey = e.shiftKey,
        altKey = e.altKey,
        ctrlKey = e.ctrlKey;
    key = parseInt(key, 10);
    if (key === 8) {
        return '[backspace]';
    }
    if (key === 91) {
        key = '[OS]';
    }
    if (key === 32) {
        key = '[space]';
    }
    if (key === 225) {
        key = '[altGr]';
    }
    if (key === 93) {
        key = '[menu]';
    }
    if (key === 20) {
        key = '[caps]';
    }
    if (key === 9) {
        key = '[tab]';
    }
    if (key === 13) {
        key = '[enter]';
    }
    if (key === 46) {
        key = '[del]';
    }
    if (key === 190) {
        key = ".";
    }
    if (key === 188) {
        key = ",";
    }
    if (key === 13) {
        key = '[enter]';
    }
    if (key === 37) {
        key = '[left]';
    }
    if (key === 40) {
        key = '[down]';
    }
    if (key === 39) {
        key = '[right]';
    }
    if (key === 38) {
        key = '[up]';
    }
    key = String.fromCharCode(key);

    if (shiftKey) {
        key = '[shift]' + key;
    }
    if (altKey) {
        key = '[alt]' + key;
    }
    if (ctrlKey) {
        key = '[ctrl]' + key;
    }
    return {
        "key": key,
        "timestamp": timestamp
    };
}

$(".container").tooltip({
    placement: "auto top"
});

// Generate stats data for the first time

function createStatsData(statsData) {
    statsData.id = 'overall';
    $.ajax({
        url: '/stats',
        type: 'POST',
        data: statsData,
    }).done(function() {
        // console.log('First created stats data');
    }).error(function(err) {
        // console.log('Cannot create statistics data');
        // console.log(err);
    });
}

/* Update stats data after calculation */

function updateStatsData(statsData) {
    $.ajax({
        url: '/stats/overall',
        type: 'PUT',
        data: statsData,
    }).done(function() {
        // console.log('Stats data updated');
    }).error(function(err) {
        // console.log('Cannot update statistics data');
        // console.log(err);
    });
}

/**
 * prepareResults fetches small statistics from Mongo and does the following:
 * 1. prepare these results for step6 to represent the user vs the average
 * 2. check whether the user is the slowest or fastest
 * 3. calculate the deviations of the user from the average
 * 4. customize the tweet button.
 * @return {null}
 */

function prepareResults(LANG) {
    // REST-interface the mongoDB server for average timings. Mongo is only accesible from 127.0.0.1
    var results;
    $.ajax({
        url: '/stats/overall',
        type: 'get',
        success: function(data) {
            // console.log('got stats data: ')
            // console.log(data);
            calculateStats(data, LANG);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            // console.log('cannot retrieve stats data');
        },
        statusCode: {
            404: function() {
                // console.log('Mongo 404ed');
                calculateStats(null);
            }
        }
    });
}

/* Recalculate the statistics based on current user data
 */

function calculateStats(results, LANG) {
    var slowest, fastest, averageTime, averageWPM, average_time_step4, participants,
        diffStep4, diffPercStep4, diffTotalTime, diffWPM, diffTotalTimeText, humanStep4, humanTime,
        diffPercStep4, isFastest, isSlowest, diffPercStep4Text, diffWPMText, diffPercTwitter;

    if (results) {
        slowest = parseFloat(results.slowest);
        fastest = parseFloat(results.fastest);
        averageTime = parseFloat(results.avg_time);
        averageWPM = parseFloat(results.avg_wpm);
        average_time_step4 = parseFloat(results.avg_step4);
        participants = parseInt(results.n, 10);
    } else {
        participants = 0;
    }

    var step4Time = parseFloat($.jStorage.get('step4ElapsedTime')), // get Elapsed time for step 4
        totalTime = parseFloat($.jStorage.get('appElapsedTime')), // get Elapsed total time

        wordCountStep4 = $.jStorage.get("step4text").length,
        milsecToMinute = 60000,
        wpm_step4 = (wordCountStep4 / step4Time * milsecToMinute).toFixed(0),
        statsData = {};

    // if first participant, create dataset
    if (participants === 0) {
        statsData = {
            // app stats
            "slowest": totalTime,
            "fastest": totalTime,
            "avg_time": totalTime,
            "n": 1,

            // step 4 stats
            "avg_wpm": wpm_step4,
            "avg_step4": step4Time, // average time at step 4
            "wpm_step4": wpm_step4
        };
        createStatsData(statsData);
    }
    // recalculate hte averages, set newest fastest or slowest times
    else {
        statsData = {
            // app stats
            "slowest": slowest > totalTime ? slowest : totalTime,
            "fastest": fastest < totalTime ? fastest : totalTime,
            "avg_time": (averageTime * participants + totalTime) / (participants + 1),
            "n": participants + 1,

            // step 4 stats
            "avg_wpm": (averageWPM * participants + wpm_step4) / (participants + 1),
            "avg_step4": (average_time_step4 * participants + step4Time) / (participants + 1), // average time at step 4
            "wpm_step4": wpm_step4
        };
        updateStatsData(statsData);
    }
    // console.log(statsData);

    // deviations
    diffStep4 = step4Time - average_time_step4;
    diffPercStep4 = (diffStep4 / average_time_step4 * 100).toFixed(2);
    diffTotalTime = ((totalTime - averageTime) / averageTime * 100).toFixed(2);

    diffWPM = ((wpm_step4 - averageWPM) / averageWPM * 100).toFixed(2);
    isSlowest = slowest < totalTime;
    isFastest = fastest > totalTime;

    totalTime = new Date(totalTime);
    diffStep4 = new Date(diffStep4);
    if (!participants) {
        $('span.for-more-than-one-person').hide();
    }
    // prop the results to the page
    if (LANG === "nl") {
        // TOTAL TIME
        humanTime = totalTime.getUTCMinutes() + " minuten en " + totalTime.getUTCSeconds() + " seconden ";
        $("#analytics-time-filler").text(humanTime);

        // only print the diff stats when there is more than 1 participant
        // console.log('n participants: ' + participants);
        if (diffTotalTime < 0) {
            diffTotalTime = (diffTotalTime * -1);
            diffTotalTimeText = diffTotalTime + "% trager dan ";
        }
        if (diffTotalTime < 1 && diffTotalTime > -1) {
            diffTotalTimeText = "exact gelijk aan ";
        } else {
            diffTotalTimeText = diffTotalTime + "% sneller dan ";
        }
        $("#analytics-time-perc").text(diffTotalTimeText);

        // TIME STEP4
        humanStep4 = diffStep4.getUTCMinutes() + " minuten en " + diffStep4.getUTCSeconds() + " seconden ";
        $("#analytics-savings-time").html(humanStep4);
        $("#analytics-savings-wpm").html(wpm_step4);

        // only print the diff stats when there is more than 1 participant
        if (diffWPM < 0) {
            diffWPM = (diffWPM * -1);
            diffWPMText = diffWPM + "% trager dan gemiddeld";
        }
        if (diffWPM < 1 && diffWPM > -1) {
            diffWPMText = "exact gelijk aan het gemiddelde!";
        } else {
            diffWPMText = diffWPM + "% sneller dan gemiddeld";
        }
        $("#analytics-savings-wpm-diff").text(diffWPMText);

        // TIME DIFFERENCE
        if (isSlowest) {
            diffPercStep4Text = "Slecht :(  Je hebt de langzaamste tijd...";
        }
        if (isFastest) {
            diffPercStep4Text = "Super goed! Je hebt de allersnelste tijd! Je bent " + diffPercStep4 + "% sneller dan gemiddeld!";
        } else {
            if (diffPercStep4 < 0) {
                diffPercStep4 = (diffPercStep4 * -1);
                diffPercStep4Text = diffPercStep4 + "% trager dan gemiddeld. Jammer de bammer.";
            }
            if (diffWPM < 1 && diffPercStep4 > -1) {
                diffPercStep4Text = "Precies gemiddeld!";
            } else {
                diffPercStep4Text = diffPercStep4 + "% sneller dan gemiddeld. Lekker lekker!";
            }
        }
        $("#analytics-comparative-result").html(diffPercStep4Text);
    }

    if (LANG === "en") {
        // TOTAL TIME
        humanTime = totalTime.getUTCMinutes() + " minutes and " + totalTime.getUTCSeconds() + " seconds";
        $("#analytics-time-filler").text(humanTime);

        // only print the diff stats when there is more than 1 participant
        if (diffTotalTime < 0) {
            diffTotalTime = (diffTotalTime * -1);
            diffTotalTimeText = diffTotalTime + "% slower than";
        }
        if (diffTotalTime < 1 && diffTotalTime > -1) {
            diffTotalTimeText = "exactly equal to";
        } else {
            diffTotalTimeText = diffTotalTime + "% faster than";
        }
        $("#analytics-time-perc").text(diffTotalTimeText);

        // TIME STEP4
        humanStep4 = diffStep4.getUTCMinutes() + " minutes and " + diffStep4.getUTCSeconds() + " seconds";
        $("#analytics-savings-time").html(humanStep4);
        $("#analytics-savings-wpm").html(wpm_step4);

        // only print the diff stats when there is more than 1 participant
        if (diffWPM < 0) {
            diffWPM = (diffWPM * -1);
            diffWPMText = diffWPM + "% slower than average";
        }
        if (diffWPM < 1 && diffWPM > -1) {
            diffWPMText = "exactly equal to the average!";
        } else {
            diffWPMText = diffWPM + "% faster than average";
        }
        $("#analytics-savings-wpm-diff").text(diffWPMText);

        // TIME DIFFERENCE
        if (isSlowest) {
            diffPercStep4Text = "Poor :(  You've got the slowest time...";
            diffPercTwitter = "eeky";
        }
        if (isFastest) {
            diffPercStep4Text = "Freaking good! You got the quickest time recorded! You're " + diffPercStep4 + "% faster than average!";
            diffPercTwitter = "amazing";
        } else {
            if (diffPercStep4 < 0) {
                diffPercStep4 = (diffPercStep4 * -1);
                diffPercStep4Text = diffPercStep4 + "% slower than average.";
                diffPercTwitter = "slow";
            }
            if (diffWPM < 1 && diffPercStep4 > -1) {
                diffPercStep4Text = "Spot on average!";
                diffPercTwitter = "average";
            } else {
                diffPercStep4Text = diffPercStep4 + "% faster than average. Nice!";
                diffPercTwitter = "good";
            }
        }
        $("#analytics-comparative-result").html(diffPercStep4Text);
    }
}

function findBootstrapEnv() {
    var envs = ['xs', 'sm', 'md', 'lg'];

    $el = $('<div>');
    $el.appendTo($('body'));

    for (var i = envs.length - 1; i >= 0; i--) {
        var env = envs[i];

        $el.addClass('hidden-' + env);
        if ($el.is(':hidden')) {
            $el.remove();
            return env;
        }
    }
}
