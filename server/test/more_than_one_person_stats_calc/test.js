module.exports = {
	'Update statistic data with more than one person' : function(test) {
		test
		// remove existing stats data
			.open('http://localhost/stats')
			.wait(100)
			.screenshot('screenshot/:browser/current_stats.png')

		// start ui test
			.open('http://localhost')
			.log.message('App timer started at ' + new Date())
			.resize({width: 1000, height:3000})
			.wait(100)
			.screenshot('screenshot/:browser/homepage.png')
			.assert.exists('#nl', 'Button #nl exists')
			.click('#nl')

		// step 0
			.waitForElement('#form-step0')
			.wait(1000)
			.assert.visible('#form-step0', 'Form for step 0 is visible')
			.screenshot('screenshot/:browser/step0.png')
			.execute(function() {
				// check the radio buttons
				radioGroups = ['handedness', 'keyboard', 'typeskill', 'englishskill', 'gender'];
				for (i in radioGroups) {
					var group = $('#form-step0 input[type="radio"][name="' + radioGroups[i] + '"]');
					$(group[Math.floor(Math.random() * group.length)]).prop('checked', true);
				}

				// choose the month dropdown
				randMonth = Math.floor(Math.random() * 12) + 1;
				$('#month option[value="' + randMonth + '"]').prop('selected', true);
			})
			.type('#jaar', '1987')
			.type('#email', 'hung@designveloper.com')
			.wait(500)
			.screenshot('screenshot/:browser/step0-filled.png')
			.click('#step0btn')

		// step 1
			.wait(1000)
			.screenshot('screenshot/:browser/step1.png')
			.assert.visible('.step1textarea', 'Step 1 text area visible')
			.type('.step1textarea', 'This is an example of the text that you are presented with.')

			// ' Below is the text area where you can type over this text. The experiment tries to find out how we can enhance typing on computers. If you are distracted, by for example kids, colleagues or friends, please click aside the text field below (for example on this text) to pause the experiment. After you\'re done, click on the "Volgende Stap" button below.')
			.screenshot('screenshot/:browser/step1-filled.png')
			.click("#step1btn")

		// step 2
			.wait(500)
			.screenshot('screenshot/:browser/step2.png')
			.assert.visible('.step2textarea', 'Step 2 text area is visible')
			.log.message('Text area 2')
			.log.message('		Started typing at ' + new Date())
			.type('.step2textarea', randomSubstring('In an interesting case, it was found that students tend to invest more often in southern companies than in northern companies. The research was held under students following economic, financial or banking studies. They speculated that soutern companies have higher returns than northern.'))
			.assert.visible('h1', 'there\' a h1 tag to click on')
			// jump out of text area after typing some text
			.log.message('		Paused typing at ' + new Date())
			.click('h1')
			.wait()
			// continue typing
			.log.message('		Resumed typing at ' + new Date())
			.type('.step2textarea', randomSubstring(' The researchers are not sure if it is related to the climate. Research at a later time might be able to show this, as global warming will diminish the differences between temperature at different sides of the country, and thus between companies located north or south. The research did however not discount for international companies.'))
			// jump out of textarea
			.log.message('		Paused typing at ' + new Date())
			.click('h1')
			.wait()
			// continue typing
			.log.message('		Resumed typing at ' + new Date())
			.type('.step2textarea', randomSubstring(' Maybe southern companies have offices in more countries. For what it\'s worth, global warming has been proven to be accelerated by humans. Which is the only true fact in this text. '))
			// jump out of textarea
			.log.message('		Stopped typing at ' + new Date())
			.click('h1')
			.wait(1000)
			.screenshot('screenshot/:browser/step2-filled.png')
			.click("#step2btn")
			.wait(500)

		// step 3
			.screenshot('screenshot/:browser/step3.png')
			.assert.visible('.step3textarea', 'Step 3 text area is visible')
			.type('.step3textarea', randomSubstring(' This is an example tekst to type over. A configuration of user interface and and amount of words predicted has been randomly picked. This might mean you have 0 words predicted, or up to 5. Try to type some words to let the prediction mechanisms work and to get a feeling for the user interface.'))
			.screenshot('screenshot/:browser/step3-filled.png')
			.wait(500)
			.click("#step3btn")
			.wait(500)

		// step 4
			.screenshot('screenshot/:browser/step4.png')
			.assert.visible('.step4textarea', 'Step 4 text area is visible')
			.log.message('Text area 4')
			.log.message('		Started typing at ' + new Date())
			.type('.step4textarea', randomSubstring(' A spiritual man provided entertainment to the businessman last season. Although the businessman didn\'t take him too serious a  first, the spirituality of the odd figure had an impressive impact.'))
			// jump out of textarea
			.assert.visible('h1', 'there\' a h1 tag to click on')
			.log.message('		Paused typing at ' + new Date())
			.click('h1')
			.wait()
			// continue typing
			.log.message('		Resumed typing at ' + new Date())
			.type('.step4textarea',randomSubstring(' The businessman become silent and actually was thinking of investing in the young man. Unfortunately, the spiritual young man had no business plan nor financial motives. He merely wanted to show the businessman that cash is not always king. That motive, however, had no impact on the businessman.'))
			// jump out of text area
			.log.message('		Paused typing at ' + new Date())
			.click('h1')
			.wait()
			// continue typing
			.log.message('		Resumed typing at ' + new Date())
			.type('.step4textarea', randomSubstring(' He directly send the spiritual man away. Although the investor was impressed with the spiritual man, he felt enough freedom by just looking at the sky. The spiritual man went along towards further development of his inner self, a balanced life, and in search for a more interested crowd. '))
			// jump out of textarea
			.log.message('		Stopped typing at ' + new Date())
			.click('h1')
			.wait(1000)
			.screenshot('screenshot/:browser/step4-filled.png')
			.wait(500)
			.click('#step4btn')
			.wait(500)

		// step 5
			.assert.visible('#form-step5',"Form step5 is visible")
			.execute(function() {
				radioGroups = ['general-working', 'speed', 'accuracy', 'improvement', 'typespeed'];
				for (i in radioGroups) {
					var group = $('#form-step5 input[type="radio"][name="' + radioGroups[i] + '"]');
					$(group[Math.floor(Math.random() * group.length)]).prop('checked', true);
				}
			})
			.type('#comments',' A spiritual man provided entertainment to the businessman last season.')
			.click('#step5btn')
			.log.message('App timer stopped at'  + new Date())
		// step 6
			.assert.visible('#step6', 'Step6 is visible')
			.wait(500)
			.screenshot('screenshot/:browser/step6.png')
			.wait(10000)
			.open('http://localhost/stats')
			.wait(100)
			.screenshot('screenshot/:browser/updated_stats.png')
			.open('http://localhost/user/last')
			.wait(100)
			.screenshot('screenshot/:browser/last_user.png')
			.done()
	}
}
function randomSubstring(s) {
	return s.substring(0, Math.floor(Math.random() * s.length));
}
