module.exports = {
	'Can run statistic calculation' : function(test) {
		test
			.open('http://localhost')
			.resize({width: 1000, height:3000})
			.wait(100)
			.screenshot('screenshot/:browser/homepage.png')
			.assert.exists('#nl', 'Button #nl exists')
			.click('#nl')
			.waitForElement('#form-step0')
			.wait(1000)
			.assert.visible('#form-step0', 'Form for step 0 is visible')
			.screenshot('screenshot/:browser/step0.png')
			.execute(function() {
				radioGroups = ['handedness', 'keyboard', 'typeskill', 'englishskill', 'gender'];
				for (i in radioGroups) {
					var group = $('#form-step0 input[type="radio"][name="' + radioGroups[i] + '"]');
					$(group[Math.floor(Math.random() * group.length)]).prop('checked', true);
				}
				randMonth = Math.floor(Math.random() * 12) + 1;
				$('#month option[value="' + randMonth + '"]').prop('selected', true);
			})
			.type('#jaar', '1987')
			.type('#email', 'hung@designveloper.com')
			.wait(500)
			.screenshot('screenshot/:browser/step0-filled.png')
			.click('#step0btn')
			.wait(1000)
			.screenshot('screenshot/:browser/step1.png')
			.assert.visible('.step1textarea', 'Step 1 text area visible')
			.type('.step1textarea', 'This is an example of the text that you are presented with. Below is the text area where you can type over this text. The experiment tries to find out how we can enhance typing on computers. If you are distracted, by for example kids, colleagues or friends, please click aside the text field below (for example on this text) to pause the experiment. After you\'re done, click on the "Volgende Stap" button below.')
			.screenshot('screenshot/:browser/step1-filled.png')
			.click("#step1btn")
			.wait(500)
			.screenshot('screenshot/:browser/step2.png')
			.assert.visible('.step2textarea', 'Step 2 text area is visible')
			.type('.step2textarea', ' In an interesting case, it was found that students tend to invest more often in southern companies than in northern companies. The research was held under students following economic, financial or banking studies. They speculated that soutern companies have higher returns than northern. The researchers are not sure if it is related to the climate. Research at a later time might be able to show this, as global warming will diminish the differences between temperature at different sides of the country, and thus between companies located north or south. The research did however not discount for international companies. Maybe southern companies have offices in more countries. For what it\'s worth, global warming has been proven to be accelerated by humans. Which is the only true fact in this text. ')
			.screenshot('screenshot/:browser/step2-filled.png')
			.click("#step2btn")
			.wait(500)
			.screenshot('screenshot/:browser/step3.png')
			.assert.visible('.step3textarea', 'Step 3 text area is visible')
			.type('.step3textarea', ' This is an example tekst to type over. A configuration of user interface and and amount of words predicted has been randomly picked. This might mean you have 0 words predicted, or up to 5. Try to type some words to let the prediction mechanisms work and to get a feeling for the user interface.')
			.screenshot('screenshot/:browser/step3-filled.png')
			.wait(500)
			.click("#step3btn")
			.wait(500)
			.screenshot('screenshot/:browser/step4.png')
			.assert.visible('.step4textarea', 'Step 4 text area is visible')
			.type('.step4textarea', ' A spiritual man provided entertainment to the businessman last season. Although the businessman didn\'t take him too serious at first, the spirituality of the odd figure had an impressive impact. The businessman become silent and actually was thinking of investing in the young man. Unfortunately, the spiritual young man had no business plan nor financial motives. He merely wanted to show the businessman that cash is not always king. That motive, however, had no impact on the businessman. He directly send the spiritual man away. Although the investor was impressed with the spiritual man, he felt enough freedom by just looking at the sky. The spiritual man went along towards further development of his inner self, a balanced life, and in search for a more interested crowd. ')
			.screenshot('screenshot/:browser/step4-filled.png')
			.wait(500)
			.click('#step4btn')
			.wait(500)
			.assert.visible('#form-step5',"Form step5 is visible")
			.execute(function() {
				radioGroups = ['general-working', 'speed', 'accuracy', 'improvement', 'typespeed'];
				for (i in radioGroups) {
					var group = $('#form-step5 input[type="radio"][name="' + radioGroups[i] + '"]');
					$(group[Math.floor(Math.random() * group.length)]).prop('checked', true);
				}
			})
			.type('#comments',' A spiritual man provided entertainment to the businessman last season. Although the businessman didn\'t take him too serious at first, the spirituality of the odd figure had an impressive impact')
			.screenshot('screenshot/:browser/step5.png')
			.click('#step5btn')
			.assert.visible('#step6', 'Step6 is visible')
			.screenshot('screenshot/:browser/step6.png')
			.wait(60000)
			.done()
	}
}
