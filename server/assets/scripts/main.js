/*jshint -W110 */
$(document).ready(function() {
    // set init variables
    $.jStorage.set('step', false);
    var MAX_STEPS = 6,
        LANG = "",
        server_request = null,
        caretPosition = {},
        userData = {},
        predictionRestartingPoint = 0,
        urlNeo4j = "http://db.autopredict.co:7474/db/data/cypher",
        doubleSpace = 0,
        // randomly generate a UI variation and prediction Depth
        UIcase = Math.floor(Math.random() * 4 + 1),
        predictionDepth = Math.floor(Math.random() * 5);

    var appTimer = new stopWatch();
    appTimer.start();
    var step2Timer = new stopWatch();
    var step3Timer = new stopWatch();
    var step4Timer = new stopWatch();
    var step2text = [];
    var step3text = [];
    var step4text = [];

    /*
     * check if user is on PC or tablet/phone
     */
    var bootstrapEnv = findBootstrapEnv();
    if (bootstrapEnv === "xs" || bootstrapEnv === "sm" || (bootstrapEnv === "md" && window.devicePixelRatio === 2)) {
        $("button#nl, button#uk, .header-start-btn").addClass("disabled");
        $("button.tabletErrorBtn").click(function() {
            enableExperimentBtns();
            $("button#nl, button#uk, .header-start-btn").removeClass("disabled");
        });
        $('#tabletModal').modal('show');
    } else {
        enableExperimentBtns();
    }

    /**
     * Listen for a key-change in the 'step'-localStorage.
     * If a change occurs, a step has changed (eg. user completed a step)
     * Then load nextStep()
     * if step == 0. then fill the previous form. great for testing and reuse of the same computer.
     */
    $.jStorage.listenKeyChange("step", function(key, action) {
        if (getStep() === 0 && !! ($.jStorage.get('user'))) {
            fillForm();
        }
        loadNextStep();
    });

    /**
     * Write first and second form results to localstorage using jStorage plugin
     */
    $(".container").on("submit", "#form-step0", function(event) {
        event.preventDefault();
        $.jStorage.set('user', {
            handedness: $("input#handedness[type=radio]:checked", this).val(),
            keyboard: $("input#keyboard[type=radio]:checked", this).val(),
            typeskill: $("input#typeskill[type=radio]:checked", this).val(),
            englishskill: $("input#englishskill[type=radio]:checked", this).val(),
            birthdate: parseInt($("select#month", this).val(), 10) + "-" + $("input#jaar", this).val(),
            gender: $("input#gender[type=radio]:checked", this).val(),
            email: $("input#email", this).val()
        });
        buttonNextStep();
    });
    $(".container").on("submit", "#form-step5", function(event) {
        event.preventDefault();
        $.jStorage.set('experiences', {
            general: $("input#general-working[type=radio]:checked", this).val(),
            speed: $("input#speed[type=radio]:checked", this).val(),
            accuracy: $("input#accuracy[type=radio]:checked", this).val(),
            improvement: $("input#improvement[type=radio]:checked", this).val(),
            typespeed: $("input#typespeed[type=radio]:checked", this).val(),
            comments: $("textarea#comments", this).val()
        });
    });

    /* pause/start indicators */
    $(".container").on("focus", "textarea.step1textarea", function(event) {
        $(".step1pause").html('');
    });
    $(".container").on("focus", "textarea.step2textarea", function(event) {
        step2Timer.start();
        $(".step2pause").html('');
    });
    $(".container").on("focus", "textarea.step3textarea", function(event) {
        $(".step3pause").html('');
        step3Timer.start();
    });
    $(".container").on("focus", "textarea.step4textarea", function(event) {
        $(".step1pause").html('');
        step4Timer.start();
    });
    $(".container").on("blur", "textarea.step1textarea", function(event) {
        if (LANG === "nl") {
            $(".step1pause").html('Gepauzeerd');
        }
        if (LANG === "uk") {
            $(".step1pause").html('Paused');
        }
    });
    $(".container").on("blur", "textarea.step2textarea", function(event) {
        step2Timer.stop();
        if (LANG === "nl") {
            $(".step2pause").html('Gepauzeerd');
        }
        if (LANG === "uk") {
            $(".step2pause").html('Paused');
        }
    });
    $(".container").on("blur", "textarea.step3textarea", function(event) {
        step3Timer.stop();
        if (LANG === "nl") {
            $(".step3pause").html('Gepauzeerd');
        }
        if (LANG === "uk") {
            $(".step3pause").html('Paused');
        }
    });
    $(".container").on("blur", "textarea.step4textarea", function(event) {
        step4Timer.stop();
        if (LANG === "nl") {
            $(".step4pause").html('Gepauzeerd');
        }
        if (LANG === "uk") {
            $(".step4pause").html('Paused');
        }
    });
    /* end pauze/start indicators */
    $(".container").on("keypress", "textarea.step2textarea", function(event) {
        step2text.push(getKey(event, step2Timer.currentTime()));
    });
    $(".container").on("loadPredict", "textarea.step3textarea, textarea.step4textarea", function(event) {
        var currentText, currentArea, firstElement, firstElementText, inpt, predictor;
        var tip = $(this).parent().find('.tip');

        if (UIcase === 2) {
            $(".experiment-text").css("margin", "15px 0 115px 15px");
        }
        if (UIcase === 4) {
            tip = $(this).parent().find('.mark_textarea');
        }

        $(this).scroll(function() {
            $(this).parent().find('.mark_textarea').scrollTop($(this).scrollTop());
        });

        if ($(this).attr("class").indexOf("step3textarea") >= 0) {
            currentText = step3text;
            currentArea = ".step3textarea";
        }
        if ($(this).attr("class").indexOf("step4textarea") >= 0) {
            currentText = step4text;
            currentArea = ".step4textarea";
        }

        $(this).on("keypress", function(event) {
            step4text.push(getKey(event, step4Timer.currentTime()));

            $(this).off('keyup').one('keyup', function(event) {
                var key_code = event.keyCode;
                if (key_code === 32) {
                    getPostitionCurrentWord($(this), key_code);
                    doubleSpace++;
                    if (doubleSpace === 2 && tip.html() !== '') {
                        rewrite_text($(this), tip, key_code);
                        doubleSpace = 0;
                        tip.html('').hide().removeClass('show');
                    }
                }
                getWord(key_code, tip, $(this));

                if ($(this).val().length < 1) {
                    // Remove value of Tip if value textarea is null or keypress is [space]
                    tip.html('').hide().removeClass('show');
                }
            });
        });
        $(this).on("keydown", function(event) {
            // console.log("key :" + event.keyCode + " timestamp :" + step4Timer.currentTime());
            autoFill($(this), tip, event);
        });
    });

    /**
     * Check for language in-use by browser
     * Bind events to the NL and UK start-buttons on the front page
     * Set LANG variables to the language in use.
     */

    function enableExperimentBtns() {
        var language = window.navigator.userLanguage || window.navigator.language;
        if (language === "nl" || language === "nl-NL" || language === "nl-BE") {
            $(".header-start-btn").attr("id", "nl");
        }
        $("button#nl").click(function() {
            loadPage("nl");
            LANG = "nl";
        });
        $("button#uk").click(function() {
            loadPage("uk");
            LANG = "uk";
        });
    }

    function getLastSentence(longSentences) {
        // split the whole text into sentences. Each special character except ' indicates new sentence
        // Each sentence is trimmed as well.
        var sentences = longSentences.toLowerCase().match(/\b\w+([^\S\n]*[\w\']+)*/ig),
            previousKeywords;
        if (sentences) {
            previousKeywords = sentences[sentences.length - 1].split(" ");
            return previousKeywords;
        }
        return null;
    }

    /**
     * Get part of the full query
     * This function takes up to 3 arguments: from, to, isKnownWords
     */

    function getQueryPartial() {
        var from,
            to,
            isKnownWords = false;
        switch (arguments.length) {
            case 0:
                return '';
            case 1:
                from = to = arguments[0];
                break;
            case 3:
                isKnownWords = arguments[2];
            default:
                from = arguments[0];
                to = arguments[1];
        }
        var partial = "";
        for (var i = from; i <= to; i++) {
            partial += "WITH ";
            for (j = 0; j < i - 1; j++)
                partial += "n" + j + ",";
            partial += "n" + (i - 1) + " ORDER BY n" + (i - 1) + ".prob DESC LIMIT 5 ";
            partial += "MATCH (n" + (i - 1) + ":`" + (i - 1) + "gram`)-[:FOLLOWED_BY]->(n" + i + ":`" + i + "gram`" + (isKnownWords ? "{word: {n" + i + "}}" : "") + ") ";
        }
        return partial;
    }

    /**
     * Get full cypher query used for RESTful API
     * return an object {query, params}
     */

    function getCypherQuery(el, key_code) {
        var query = '',
            gram = 0,
            limit_word = 5,
            previousKeywords = getLastSentence(el.val());
        // console.log("l267: " + previousKeywords);

        if (previousKeywords.length < predictionRestartingPoint) {
            // new sentence has found
            predictionRestartingPoint = 0;
        } else {
            // remove all words before prediction restarting point
            previousKeywords.splice(0, predictionRestartingPoint);
        }
        // console.log("l276: " + previousKeywords);
        if (previousKeywords) {

            for (i in previousKeywords) {
                // escape the ' character such that neo4j does not complain as bad request
                previousKeywords[i] = previousKeywords[i].replace(/(\')/g, "\\$1");
            }
            gram = (previousKeywords.length - 1) % (predictionDepth + 1);
            if (gram == -1) {
                return null;
            }
            var start = previousKeywords.length - 1 - gram;
            var params = {};
            // query full sentence

            query = "MATCH (n0:`0gram`) ";
            if (gram > 0) {
                // at least one word has been typed
                for (var i = 0; i < gram; i++) {
                    // known words
                    params['n' + i] = previousKeywords[start + i];
                }

                query += "USING INDEX n0:`0gram`(word) WHERE n0.word={n0} ";
                query += getQueryPartial(1, gram - 1, true);

                // word being typed
                query += getQueryPartial(gram) + "WHERE n" + gram + ".word =~ {typing_word} ";
                params.typing_word = '(?i)' + previousKeywords[start + gram] + '.*';
                query += getQueryPartial(gram + 1, predictionDepth);
            } else {
                // only the 0gram word being typed at this point
                query += "WHERE n0.word =~ {n0} ";
                params['n0'] = "(?i)" + previousKeywords[start] + ".*";
                query += getQueryPartial(1, predictionDepth);
            }

            // return clause
            query += "RETURN ";
            for (var i = gram; i < predictionDepth; i++) {
                query += "n" + i + ".word + ' ' + ";
            }
            query += "n" + predictionDepth + ".word AS predictedSentence ORDER BY n" + predictionDepth + ".prob DESC LIMIT 5";
        }
        return {
            query: query,
            params: params
        };
    }

    function getWord(key_code, tip, el) {
        var active = check_key_press(key_code);
        if (server_request) {
            if (UIcase === 4) {
                tip.hide().removeClass('show');
            }
            server_request.abort();
        }
        if (active) {
            doubleSpace = 0;
            var cypherQuery = getCypherQuery(el, key_code);
            if (cypherQuery) {
                // console.log(cypherQuery);
                server_request = $.ajax({
                    url: urlNeo4j,
                    type: "POST",
                    /*  accepts: "application/json; charset=UTF-8", */
                    accept: "application/json; charset=UTF-8",
                    dataType: "json",
                    contentType: "application/json",
                    data: JSON.stringify(cypherQuery),
                    success: function(wordsPredicted) {
                        // console.log(wordsPredicted.data);
                        switch (wordsPredicted.data.length) {
                            case 0:
                                tip.html("").hide().removeClass('show');
                                predictionRestartingPoint = (getLastSentence(el.val())).length;
                                // console.log("No prediction found, will restart from 0gram after a space or any special character entered");
                                break;
                            default:
                                var html = "<li class='focus'>" + wordsPredicted.data[0][0] + "</li>";
                                for (var i = 1; i < wordsPredicted.data.length; i++) {
                                    html += "<li>" + wordsPredicted.data[i][0] + "</li>";
                                }
                                if (UIcase === 4) {
                                    tip.html(wordsPredicted.data[0][0]).css({
                                        color: "transparent"
                                    });
                                    if (el.val().length > 1) {
                                        var lengthofCurrentText = el.val().length;
                                        var current_sentence = el.val().split(" "),
                                            html1 = "",
                                            numword = current_sentence.length - 1;
                                        for (var i = 0; i < numword; i++) {
                                            html1 += current_sentence[i] + ' ';
                                        }
                                        var tempText = (html1 + wordsPredicted.data[0][0]).substring(lengthofCurrentText, (html1 + wordsPredicted.data[0][0]).length);
                                        tip.html(el.val() + "<span style='color:white; background:#008CFE'>" + tempText + "</span>").show().addClass('show');
                                    }
                                } else {
                                    tip.html("<ul>" + html + "</ul>").show().addClass('show');
                                    getPostitionCurrentWord(el, key_code);
                                }
                                break;
                        } // end default
                    }, // end success
                    error: function(xhr, err, msg) {
                        // console.log(xhr);
                        // console.log(err);
                        // console.log(msg);
                    }
                });
            }
        }
    }

    // return true if alphabetical key or single quote pressed

    function check_key_press(key_code) {
        return ((65 <= key_code) && (90 >= key_code)) // alphabet
        || (48 <= key_code) && (57 >= key_code) // number
        || key_code === 222; // single quote
    }

    function autoFill(el, tip, e) {
        var key_code = e.keyCode;
        if (tip.hasClass('show')) {
            switch (true) {
                case key_code === 39 || key_code === 13 || key_code === 9: //Right - Enter - Tab
                    rewrite_text(el, tip, key_code);
                    tip.hide().removeClass('show');
                    e.preventDefault();
                    doubleSpace = 0;
                    break;
                case key_code === 38: //Up
                    e.preventDefault();
                    select_text_predict(tip, "-");
                    doubleSpace = 0;
                    break;
                case key_code === 40: //Down
                    select_text_predict(tip, "+");
                    doubleSpace = 0;
                    break;
            }
        }
    }

    // If key "+" select  word predicted down, "-" select word predicted up

    function select_text_predict(tip, key) {
        var index = tip.find('li.focus').index(),
            temp = index,
            num_word = tip.find('li').length;
        switch (key) {
            case "+":
                temp++;
                break;
            case "-":
                temp--;
                break;
        }
        var next_word = temp % num_word;
        tip.find('li').removeClass('focus');
        tip.find('li:eq(' + next_word + ')').addClass('focus');
    }

    /*update value for textarea after  words predicted is selected*/

    function rewrite_text(el, tip, key_code) {
        switch (UIcase) {
            case 4:
                el.val(tip.text() + " ");
                break;
            default:
                var current_sentence = el.val().trim().split(" ");
                var html = "";
                for (var i = 0; i < current_sentence.length - 1; i++) {
                    html += current_sentence[i] + ' ';
                }
                if(doubleSpace === 2) {
                    el.val(html + tip.find('li.focus' + " ").html() + " ");
                }
                else {
                    el.val(html + tip.find('li.focus' + " ").html());
                }
                getPostitionCurrentWord(el, key_code);
                break;
        }
    }

    /**
     * getPostitionCurrentWord gets the position of the current word being typed
     * @param  {String} el       the element (textarea) in which is being typed
     * @param  {int} key_code    the keycode being typed
     * @return {null}
     */

    function getPostitionCurrentWord(el, key_code) {
        var markTextarea = el.parent().find('.mark_textarea');
        if (UIcase !== 4) {

            var selection = el.getSelection();
            var index = selection.start;
            var tip = el.parent().find('.tip');
            var range = rangy.createRange();
            markTextarea.text(el.val());

            if (typeof markTextarea[0].childNodes[0] !== 'undefined') {
                range.setStart(markTextarea[0].childNodes[0], index);
                var caret = $("<span class='text_cursor'></span>")[0];
                range.insertNode(caret);
                caretPosition = el.parent().find(".text_cursor").position();
                updatePositionTip(tip, key_code);
            }
        }
    }

    /**
     * updatePositionTip sets the position of the predictionbox/tooltip
     * reflective of the characters being typed
     * This assures that the predeictionbox is kept afloat with the cursor
     * @param  {String} tip      the prediction-box
     * @param  {int} key_code    the keycode being typed
     * @return {null}            re-postition the prediction-box
     */

    function updatePositionTip(tip, key_code) {
        if (key_code === 32 || UIcase === 3) {
            switch (UIcase) {
                case 1:
                    tip.css({
                        top: (caretPosition.top + 16),
                        left: (caretPosition.left + 18)
                    });
                    break;
                case 2:
                    tip.css({
                        top: (caretPosition.top - tip.outerHeight(true)),
                        left: (caretPosition.left + 18)
                    });
                    break;
                case 3:
                    tip.css({
                        top: (caretPosition.top),
                        left: (caretPosition.left + 28)
                    });
                    break;
            }
        } else if (UIcase === 2) {
            tip.css({
                top: (caretPosition.top - tip.outerHeight(true))
            });
        }
    }

    /**
     * createUI creates the randomized UI in a specific textarea
     * @param  {String} el the textarea
     * @return {null}
     */

    function createUI(el) {
        el.wrap("<div class='wrapper_textarea'></div>");
        $('<div id="markTextarea' + el.data('step') + '" class="mark_textarea"></div>').insertBefore(el);
        $('<div id="tip_' + el.data('step') + ' "class="tip"><ul></ul></div>').insertBefore(el);
//        if (UIcase !== 4) {
//            $('#markTextarea' + el.data('step')).width(el.width());
//        }
    }

    /**
     * getStep returns the step to be executed.
     * it discounts for no step (users need to start the experiment)
     *
     * @return {int} the next step to be loaded
     */

    function getStep() {
        var currentStep = $.jStorage.get('step');
        if (currentStep === false || !isNumeric(currentStep)) {
            return 0;
        } else {
            return currentStep;
        }
    }

    /**
     * ButtonNextStep gets called whenever someone clicks a button for a next step
     * It gets the currentStep, parses functions before firing off the next step
     * *** ButtonsNextStep is the final act before loading a new step. ***
     *
     * @return {null}
     */

    function buttonNextStep() {
        // get the current step
        var step = getStep();
        // reset the pause stage
        $("#stepPause").html('');

        // when step 2 is finished, we check for empty textarea
        // store the timings and the text
        if (step === 2) {
            if ($("textarea.step2textarea").val() == ""){
                return false;
            }

            $.jStorage.set('step2ElapsedTime', step2Timer.getElapsedMilliseconds());
            $.jStorage.set('step2text', step2text);
        }

        // when step 3 is finished we simulate timings being recorded.
        // any lag/latency/delay will be fetched this way
        if (step === 3) {

            $.jStorage.set('step3ElapsedTime', step3Timer.getElapsedMilliseconds());
            $.jStorage.set('step3text', $('textarea.step3textarea').val());
        }

        // when step 4 is finished we check for actual data
        // store timings and the text
        if (step === 4) {
            // prevent people from not enetering text and proceeding to step5
            if ($("textarea.step4textarea").val() == "") {
                return false;
            }
            $.jStorage.set('step4ElapsedTime', step4Timer.getElapsedMilliseconds());
            $.jStorage.set('step4text', step4text);
        }

        if (step === 5) {
            appTimer.stop();
            appTimer.printElapsed();
            $.jStorage.set('appElapsedTime', appTimer.getElapsedMilliseconds());
            pushUserDataToServer();
            prepareResults(LANG);
        }
        // set the next step and thus load it
        $.jStorage.set("step", step + 1);
    }

    /**
     * grap data from local storage and send to nodejs server
     * @return {null}
     */

    function pushUserDataToServer() {
        var setup = predictionDepth + 'gram ';
        switch (UIcase) {
            case 1:
                setup += 'box-below';
                break;
            case 2:
                setup += 'box-above';
                break;
            case 3:
                setup += 'box-right';
                break;
            case 4:
                setup += 'text-inline';
                break;
        }

        userData = {
            "info": $.jStorage.get('user'),
            "experience": $.jStorage.get('experiences'),
            "config": {
                UIcase: UIcase,
                predictionDepth: predictionDepth
            },
            "step2": {
                start: step2Timer.startTime,
                stop: step2Timer.stopTime,
                duration: step2Timer.getElapsedMilliseconds(),
                text: $.jStorage.get('step2text')
            },
            "step4": {
                setup: setup,
                start: step4Timer.startTime,
                stop: step4Timer.stopTime,
                duration: step4Timer.getElapsedMilliseconds(),
                text: $.jStorage.get('step4text')
            },
            "appTimer": {
                start: appTimer.startTime,
                stop: appTimer.stopTime,
                duration: appTimer.getElapsedMilliseconds()
            }
        }

        $.ajax({
            url: '/user',
            type: 'POST',
            data: userData,
        }).done(function() {
            // console.log('pushed data to server');
        })
    }


    /**
     * loadNextStep loads the next step,whilst fading the currentstep out
     * after that, it sets the progressbar
     * *** loadNextStep is the first act in loading a step. ***
     * @return {null}
     */

    function loadNextStep() {
        var step = getStep();
        var stepId = "#step" + step;
        // animate the switching of steps. slide-in and out
        $(".currentStep").animate({
            left: -$(".currentStep").outerWidth()
        }).removeClass('currentStep').addClass('hide');
        $(stepId).css({
            "left": -1000,
            "position": "relative"
        }).removeClass("hide").animate({
            left: 0
        }).addClass('currentStep');
        // assure that participants see the top of the page
        $('html, body').animate({
            scrollTop: $(stepId).offset().top - 45
        }, 10);
        // end of animations

        // Only get the user configuration in step 3,
        // step uses configuration gained from step 3
        if (step === 3) {
            createUI($("textarea.step3textarea"));
            createUI($("textarea.step4textarea"));
            $(window).resize(function() {
                $('#markTextarea3').width($('.step3textarea').width());
                $('#markTextarea4').width($('.step4textarea').width());
            });
            $("textarea.step3textarea,textarea.step4textarea").trigger("loadPredict");
        }
        setProgress();
    }

    /**
     * setProgress sets the progress thus far in the progressbar
     * it utilizes MAX_STEPS to determine the process in percentage
     *
     */

    function setProgress() {
        var currentStep = getStep();
        var progress;
        if (currentStep === 0) {
            progress = 5;
        } else {
            progress = currentStep / MAX_STEPS * 100 + "%";
        }
        $(".progress-bar").attr("aria-valuenow", progress).animate({
            width: progress
        }, "slow");
        $(".progress-bar .sr-only").html(progress + "% Complete");
    }

    /**
     * loadPage loads the current language experiment pages in.
     * a page is a single HTML file that includes all steps for the experiment.
     * This allows for caching and
     * no further network traffic for browising through the steps
     *
     * @param   {string} lang   a language identifier refering to an html file
     * @return    {null}
     */

    function loadPage(lang) {
        $(".page").children().fadeOut();
        $(".page").load('pages/' + lang + '.html', function(response, status, xhr) {
            if (status == "error") {
                var msg = "Sorry but there was an error: ";
                $("#error").html(msg + xhr.status + " " + xhr.statusText);
            } else {
                $.jStorage.set('step', 0);
                $("#form-step0").validate({
                    debug: true,
                    errorPlacement: function(error, element) {
                        error.appendTo(element.parents("div.question-container").find("div.well"));
                    },
                    errorClass: "has-error alert-danger",
                    validClass: "has-success",
                    highlight: function(element, errorClass, validClass) {
                        $(element).addClass(errorClass).removeClass(validClass);
                        $(element).parents("div.question-container").find("div.well").addClass(errorClass);
                        $(element).parents("div.question-container").find("div.input-group").addClass(errorClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).removeClass(errorClass).addClass(validClass);
                        $(element).parents("div.question-container").find("div.well").removeClass(errorClass);
                        $(element).parents("div.question-container").find("div.input-group").removeClass(errorClass).addClass(validClass);
                    }
                });
                $("button.nextStep").click(function() {
                    buttonNextStep();
                });
                $('.well, textarea').bind('copy paste cut drag drop contextmenu', function(e) {
                    e.preventDefault();
                });
            }
        });
    }
});
